---
title: OS package
weight: 8
---

# OS package

An OS package is composed of two files: an _archive_ and a _descriptor_.  The
archive contains a Linux kernel, an initramfs, and a kernel command line.  The
descriptor is a detached signature on the compressed archive.

## Archive

All archives include a file named `manifest.json`.  This file contains key-value
pairs in JSON format, listing settings and file names that can be located in the
archive.  A bootloader can hand over control to the OS package by passing the
specified settings and files to the kexec system call.

| Field     | Type    | Required | Short description                      |
|-----------|---------|----------|----------------------------------------|
| version   | integer | yes      | Version of the manifest format         |
| initramfs | string  | yes      | File name of an initramfs image        |
| kernel    | string  | yes      | File name of a kernel image            |
| cmdline   | string  | no       | Kernel command line to be used on boot |
| label     | string  | no       | Arbitrary OS package description       |

The entire archive directory is compressed using the ZIP algorithm and stored as
a `.zip` file.

### **version**

The version field is a monotonic counter.  It is incremented on breaking changes
to the manifest format.  The current version number is `1`.

### **initramfs**

The initramfs field is a file name of an initramfs image stored in the archive.
For example, "st.initrd" if the image is stored as "st.initrd" in the archive's
root.

### **kernel**

The kernel field is a file name of a kernel image stored in the archive.  For
example, "kern/st.vmlinuz" if the image is stored as "st.vmlinuz" in the
archive's "kern" directory.

### **cmdline**

The cmdline field is the kernel command line to use on boot.  For example,
"console=ttyS0,115200n8 ro rdinit=/lib/systemd/systemd systemd.log_level=debug".

### **label**

The label field is an arbitrary description of the OS package.  There is no
defined behavior on how software should make use of the specified label.
Consider any specified label to be non-essential debug information.

### Example

```json
{
  "version": 1,
  "label": "testonly",
  "initramfs": "boot/initrd.img-4.19.0-16-amd64",
  "kernel": "boot/vmlinuz-4.19.0-16-amd64",
  "cmdline": "BOOT_IMAGE=/boot/vmlinuz-4.19.0-16-amd64 root=UUID=3b0f1b1f-1b1f-1b1f-1b1f-1b1f1b1f1b1f ro quiet"
}
```

## Descriptor

The descriptor is a threshold signature on the zip archive, expressed as
key-value pairs in JSON format.  The "threshold signature scheme" is just
multiple Ed25519 signatures that are produced by different signing keys.

| Field        | Type            | Required | Short description                          |
|--------------|-----------------|----------|--------------------------------------------|
| version      | integer         | yes      | Version of the descriptor format           |
| signatures   | list of strings | no       | Zero or more signatures on the zip archive |
| certificates | list of strings | no       | Zero or more signing certificates          |
| os_pkg_url   | string          | no       | HTTP(S) URL of the signed zip archive      |

Please note that even though it is optional to provide os_pkg_url, signatures,
and certificates from a parsing perspective, downloading and verification is
doomed to fail unless these fields are set with valid values.

### **version**

The version field is a monotonic counter.  It is incremented on breaking changes
to the descriptor format.  The current version is `1`.

### **signatures**

The signatures field is a list of signatures on the zipped archive.  Each
signature is in [base64][].  The number of signatures and their order must match
the certificates field to facilitate correct verification, see below.

Each signature must be computed as follows:

  1. Hash the zipped archive using SHA256.
  2. Sign the resulting 32 bytes using [Ed25519][] to get a 64-byte signature.

[base64]: https://www.rfc-editor.org/rfc/rfc4648.html#section-4
[Ed25519]: https://www.rfc-editor.org/rfc/rfc8032.html

### **certificates**

The certificates field is a list of X.509 certificates.  Each certificate is in
PEM format and then [base64][] encoded.  Refer to the [trust
policy](./trust_policy) documentation for criteria on how to validate these
certificates and signatures.

### **os_pkg_url**

The OS package URL field specifies where the zip archive can be downloaded using
HTTP(S).  For example, `https://ospkg.example.org/debian-stable.zip`.

If the URL is not absolute (i.e., not starting with scheme and host), it
is resolved using the the URL of the descriptor file as the "base URI".
E.g., if the descriptor file was downloaded from
`https://ospkg.example.org/debian-stable.json`, and the OS package URL
in the descriptor is `debian-stable.zip`, that is resolved to
`https://ospkg.example.org/debian-stable.zip` before download. See
[Reference Resolution](https://www.rfc-editor.org/rfc/rfc3986#section-5)
for the precise resolution rules.

**Compatibility note**: ST docs version v0.2.5 and earlier, part of the
st-1.0.0 collection release, did not specify any "base URI" to use for
resolving relative URLs, and hence implicitly required an absolute URL
for the `os_pkg_url`. When compatibility with the st-1.0.0 components is
desired, avoid using a relative URL in the descriptor.

### Example

```json
{
  "version": 1,
  "os_pkg_url": "https://ospkg.example.org/debian-stable.zip",
  "signatures": [
    "PKXDgecGrGHGglYkq0rllVd7IWTG4FOLE0cTfEPukUM4eQTiQvn/ljgWDsoSUXyxRbIqptpek1F4Nzc5mQrvCA==",
    "k+ud2g6G+G9dCuGCg1fNkOzkSAQzxCc01xIL/snSaALh5I3F16jFvENZ5HZ4D1/iQB87I2ugEkmub2Gy1b7jAQ=="
  ]
  "certificates": [
    "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSFBNSUdDb0FNQ0FRSUNFQlEzMWdmc0E5Q1ROWnNYWmJoU2hqY3dCUVlESzJWd01BQXdIaGNOTWpNd09UQTQKTVRZME9EVTRXaGNOTWpNd09URXhNVFkwT0RVNFdqQUFNQ293QlFZREsyVndBeUVBdXVBbGE5eEZObmlyWHRYdgpOalQrTVJwYXB5RzBmbExBME54NWxNTWRYNmFqRWpBUU1BNEdBMVVkRHdFQi93UUVBd0lIZ0RBRkJnTXJaWEFEClFRRFl4WlFDWHM0UlJ4ajFEUWNUQ2JjZGdGY1prZUJoSW9FcFRtMFplMDJOUmZQR1JDcUFRYWJnYjREUmhRMXcKODdQdDNXOEFuTDJBY0JBMjd6VkhFUW9OCi0tLS0tRU5EIENFUlRJRklDQVRFLS0tLS0K",
    "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSFBNSUdDb0FNQ0FRSUNFQ3piTTZCN1QrMmh3VkZGdEYxVTI1VXdCUVlESzJWd01BQXdIaGNOTWpNd09UQTQKTVRZME9EVTRXaGNOTWpNd09URXhNVFkwT0RVNFdqQUFNQ293QlFZREsyVndBeUVBeWxXdFlBRHdKTERiNGxHbwp3amlCN1dJM1lGS2pSYUZBemJCaWVBRi9vZm1qRWpBUU1BNEdBMVVkRHdFQi93UUVBd0lIZ0RBRkJnTXJaWEFEClFRQS85UXAwSW5wd1BMVXJwWUF6am5mTWpDeHFZL3ZobTROUjNXemU5UXg4eHh0UkJVS3VQRGFNL1VmTGtGSGMKZ1NFdU92UUpNUUIvWjBianhUUzNHOTRLCi0tLS0tRU5EIENFUlRJRklDQVRFLS0tLS0K"
  ],
}
```
