---
title: System Transparency
weight: 1

---

# System Transparency

*Documentation for System Transparency version 1.1.0.*

----

## Getting started

- [List of components](docs/introduction/components/) in this release
- [Quickstart guide](docs/introduction/quickstart/) to boot a System Transparency OS package
- [Build guide](docs/introduction/build/) to build your own transparent system
- [How to contribute](docs/contribute/) to System Transparency

## External links

- [ST source code repositories](https://git.glasklar.is/system-transparency)
- [Community chat room](https://matrix.to/#/#system-transparency:matrix.org)
- Mailing Lists
  - [ST Announce](https://lists.system-transparency.org/mailman3/postorius/lists/st-announce.lists.system-transparency.org/)
  - [ST Discuss](https://lists.system-transparency.org/mailman3/postorius/lists/st-discuss.lists.system-transparency.org/)
- [Weekly Meetings](https://meet.glasklar.is/ST-weekly) on Mondays at 12:00 UTC
