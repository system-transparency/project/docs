---
title: Why ST?
weight: 2
---

# Why should I use System Transparency?

In today's digital landscape, the trustworthiness of our systems is paramount. System Transparency (ST) represents a commitment to this trust, ensuring that from the moment a computer is powered on, it operates only with software that is genuine, unaltered, and intended for use. To realize the full potential of ST and guarantee a safe boot process, certain foundational system requirements need to be met.

System Transparency is designed to be a secure and open solution to run your data center. The project aims to provide a complete toolbox for server operators securely manage their lifecycles. By building everything in a reproducible way [reproducible builds](https://reproducible-builds.org/) you can verify the source code matches the prebuilt artifacts.

If you are operating Linux with iPXE or any network boot solution in your data center you should consider to use System Transparency. It will streamline your processes and reduce the maintenance costs. It will also improve your security posture since the security is built in by design.

## Value proposition we might provide to you:

- **Deterministic Deployment:**
  - The same image will always result in the same deployment.
  - Know what software versions are running on your systems
  - Good known state is just a reboot away
  - Make security audits more efficient (look at how the images are created)
  - Easy upgrades and rollbacks
  - Drastically reduce the cost of clean-up associated with security intrusions
- **Secure by Default:**
  - Comes with security features built-in by experts such as
    - Signature Verification
    - Minimal Trusted Computing Base
- **Customization:**
  - The entire infrastructure can be customized to your needs.
- **Open Source:**
  - The entire infrastructure is open source, reproducible and easy maintainable.
- **Extensive Hardware Compatibility:**
  - Reduces driver issues and increases interoperability.
