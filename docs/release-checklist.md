# Release checklist

## Making a release

  - [ ] Prepare other components first (stboot, stmgr, stprov)
  - [ ] docs/release-checklist.md looks reasonable
  - [ ] docs/testing-docs.md looks reasonable
  - [ ] RELEASES.md is up-to-date, see expectations below
  - [ ] README.md is up-to-date
  - [ ] Create a pre-release tag
  - [ ] All release tests pass, see [test docs][]
  - [ ] After finalizing the NEWS file (see expectations below), create a new
    signed tag.  Usually, this means incrementing the third number for the most
    recent tag that was used during the above release testing.

[test docs]: ./testing-docs.md
[st-announce]: https://lists.system-transparency.org/mailman3/postorius/lists/st-announce.lists.system-transparency.org/

## RELEASES-file

  - [ ] What in the repository is released and supported
  - [ ] The overall release process is described, e.g., where are releases
    announced, how often do we make releases, what type of releases, etc.
  - [ ] The expectation we as maintainers have on users is described
  - [ ] The expectations users can have on us as maintainers is
    described, e.g., what we intend to (not) break in the future or any
    relevant pointers on how we ensure that things are "working".

## NEWS-file

  - [ ] The previous NEWS entry is for the previous release
  - [ ] Explain what changed, e.g., specification updates, major additions or
    reorganization, etc.
  - [ ] List which components the site was build-tested with

Note that the NEWS file is created manually from the git-commit history.
