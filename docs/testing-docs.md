# Testing docs

This file documents how docs is tested:

  - Checked-in content is up to date
  - The site builds as expected

We do these tests before cutting a docs release.  Be warned that there is no
automation, and that there is a degree of subjectivity involved here.

## Checked-in content is up to date

Check that any checked in reference documentation is consistent.  For example,
there are no inconsistencies between the host configuration, trust policy, and
OS package documentation.

Check that any checked in guides work by trying them interactively.  For
example, the quickstart guide works when following it step-by-step.  Note that
this includes getting the documented stimages and stmgr versions bumped.

The st collection version is used in quickstart.md: bump it manually.

Make an overhaul of any other checked in content.  Ensure that nothing is
obviously wrong or particularly unhelpful.

## The site builds as expected

We check that it is possible to build the site in the context of a collection
release.  Ensure there is a pre-release tag of docs, clone the release
engineering repository, and create a manifest with the components under test.

    $ git clone --depth 1 https://git.glasklar.is/system-transparency/core/system-transparency.git /tmp/st
    $ cd /tmp/st
    $ (cd collection-releases && cp st-1.0.0.manifest st-X.Y.Z.manifest)

Edit the copied manifest file to list the appropriate component versions.

Create the tarball.  Use the force flag to allow not having signatures.

    $ ./releng/mk-release-archive.sh -f -o /tmp/st-X.Y.Z \
        collection-releases/st-X.Y.Z.manifest \
        collection-releases/NEWS

You should see that `/tmp/st-X.Y.Z{,.tar.gz}` was created.  Now we are ready to
test that the site builds as expected with the listed manifest components.

    $ (cd /tmp && rm -rf st-X.Y.Z && tar xzf st-X.Y.Z.tar.gz)
    $ cd /tmp/st-X.Y.Z/docs
    $ ./assemble.sh
    $ hugo serve

The site is being rendered at http://localhost:1313.  Eyeball all pages from
start to finish.  In particular, check that:

  - [ ] There are no empty pages
  - [ ] There are no placeholder pages that were not substituted
  - [ ] All links work (click them)
  - [ ] Links to material that is rendered on the site use relative filenames,
    i.e., when clicking such links the site continues to be browsed locally.
