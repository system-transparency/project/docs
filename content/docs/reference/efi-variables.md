---
title: EFI variables
weight: 10
---

# EFI variables

UEFI-compatible systems may have some configuration in NVRAM as EFI variables.
The variable names are prefixed with "ST" and use GUID
"f401f2c1-b005-4be0-8cee-f2e5945bcbe7".  If a variable is read by a
byte-oriented tool, be aware that the first four bytes correspond to EFI-NVRAM
metadata and not variable data.

| Variable name | Variable data / format                                    |
|---------------|-----------------------------------------------------------|
| STHostConfig  | [Host configuration](./host_configuration.md)             |
| STHostName    | Arbitrary string                                          |
| STHostKey     | Unencrypted Ed25519 key in [OpenSSH private-key format][] |


[OpenSSH private-key format]: https://github.com/openssh/openssh-portable/blob/master/PROTOCOL.key
