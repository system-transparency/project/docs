---
title: Contribute
weight: 10
bookCollapseSection: true
---

# Contribute to ST

Contributions to the System Transparency project are most welcome.  Examples of
contributions include testing the project's software, filing issues, and opening
merge requests that fix bugs or add features.  If you are unsure where to start
contributing, considering reaching out on IRC, Matrix, or join a weekly Jitsi
meet.

Some of the central repositories you might want to contribute to include:

  - The ST bootloader: [core/stboot](https://git.glasklar.is/system-transparency/core/stboot/)
  - Tool for provisioning a new platform: [core/stprov](https://git.glasklar.is/system-transparency/core/stprov/)
  - Tool for creating various images: [core/stmgr](https://git.glasklar.is/system-transparency/core/stmgr)
  - Example scripts to get started with ST: [core/stimages](https://git.glasklar.is/system-transparency/core/stimages)
  - Project proposals: [project/documentation](https://git.glasklar.is/system-transparency/project/documentation/-/tree/main/proposals)
  - ST documentation (this site): [project/docs](https://git.glasklar.is/system-transparency/project/docs)

Refer to the README file in each repository for more information.
