---
title: System Requirements
weight: 3
---

# System Requirements

To harness the full potential of System Transparency (ST) and ensure a seamless and secure boot process, specific hardware and software prerequisites are essential.

1. **Processor**: *x86_64* architecture processor. This is vital for compatibility at the moment.
2. **Secure Boot Capability**: Hardware that supports a secure boot process, ensuring only signed and trusted software is executed at startup.
3. **TPM 2.0 (Trusted Platform Module)**: A hardware component to securely store cryptographic keys, enhancing system security.
4. **Network Access**: For receiving updates, performing remote attestation, and other essential operations.

Ensure your system meets these requirements to fully leverage the benefits of ST.

