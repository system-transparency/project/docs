---
title: What is ST?
weight: 1
---

# What is ST?

ST, or System Transparency, is a security framework ensuring your computer starts up safely and reliably. Think of it as a guarantee: when your computer powers on, it uses only trusted software, free from tampering or malicious changes. ST achieves this by:

1. **Reproducible Builds**: Ensuring software is built consistently from its source.
2. **Immutable Infrastructure**: Making sure software changes are traceable and verifiable.
3. **Remote Attestation**: Allowing external checks to confirm the software's integrity.

In essence, ST gives you confidence in the software that runs your system from the moment you switch it on.

## Glasklar Teknik Implementation

System Transparency is a bootloader distribution that lets you quickly and securely maintain your data center. It runs as [LinuxBoot bootloader](https://linuxboot.org) on every UEFI/EDK2 firmware supported device. At a high level, it sits between the server hardware and operating system to ensure that only software that users expect and trust is booted on the host.

System Transparency aims to provide a toolbox that can be used to securely operate server fleets from the iron and up.

![](/st-overview.png)

The System Transparency project provides a variety of components that can be used together for secure/trustworthy data center operations. Several of these are optional and can be excluded/replaced depending on your preferences, requirements and threat-model. The cornerstone of System Transparency and only mandatory component is the bootloader, which is signed with a key that is embedded in the firmware of your server. This ensures that the bootloader is not tampered with and can be launched on any UEFI Secure Boot enabled platform.

![](/st-package.png)

We provide the core component of ST to you: [the bootloader](https://git.glasklar.is/system-transparency/core/stboot).

We also give you the tools:

1. [to build your signed OS images](https://git.glasklar.is/system-transparency/core/stmgr)
2. [manage key material and signatures](https://git.glasklar.is/system-transparency/project/sthsm)
3. [provision your platform with ST](https://git.glasklar.is/system-transparency/core/stboot)
4. [operate your data center with ST](https://git.glasklar.is/system-transparency/core/stprov)

The minimal required services to run your data center with ST are just the signed bootloader.
