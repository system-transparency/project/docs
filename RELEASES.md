# Releases of docs

## What is being released

  - The Hugo source code of the entire repository, except for the
    `content/archive` directory which contains outdated material.  Some outdated
    material may also be lingering in the `static` directory.

What changed in each release is documented in a [NEWS](./NEWS) file.  The NEWS
file also specifies which other System Transparency components are meant to be
used when building the site with content copied straight into the Hugo tree.

Note that a release is simply a signed git-tag with a matching NEWS entry,
accessed from the [docs][] repository.  There are no release emails on the
[st-announce][] list because they would mostly be 1:1 with collection releases.

To verify tag signatures, get the allowed-ST-release-signers file published at
the [signing-key page][], and verify the tag vX.Y.Z using the following command:

    git -c gpg.format=ssh -c gpg.ssh.allowedSignersFile=allowed-ST-release-signers tag --verify vX.Y.Z

The above configuration can be stored permanently using `git config`.

Users are mainly expected to interact with released versions of the docs
component as part of collection releases and `docs.system-transparency.org`.

[docs]: https://git.glasklar.is/system-transparency/project/docs
[st-announce]: https://lists.system-transparency.org/mailman3/postorius/lists/st-announce.lists.system-transparency.org/
[signing-key page]: https://www.system-transparency.org/keys/

## Release testing

See the [test docs](./docs/testing-docs.md) for information on how we check that
the site in this repository builds and works as expected. 

## What release cycle is used?

We make releases when new documentation is ready and meant for rendering at
`www.docs.system-transparency.org`.  In other words, a releases of the docs
component is tightly coupled with a corresponding [collection release][].

[collection release]: ./content/docs/releases/collection-release.md

## Upgrading

The internal structure of the Hugo tree may be changed across releases without
warning.  For example, files and directories could be deleted or moved.  If you
depend on docs from anywhere but `docs.system-transparency.org/st-vX.Y.Z`, then
ensure to pin the version with a git-commit or git-tag to avoid breakage.  Refer
to the git-history and the NEWS file to figure out how to upgrade appropriately.
