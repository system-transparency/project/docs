---
title: How to create a signed OS package
---

# How to create a signed OS package

This guide shows you how to create an OS package that is signed with your keys and ready to be fetched/booted by stboot.

In order to build up an OS package you need to provide a Linux kernel of your choice and an initramfs containing the userspace for your service.
Utilize the stmgr tool to package them:
```bash
stmgr ospkg create -label='System Transparency Example OS' \
-kernel=path/to/your-OS-kernel \
-initramfs=path/to/your-OS-initramfs \
-cmdline='console=tty0' 
```
Use the `-label` option to add a short description of the boot configuration. With `-cmdline` you can define the command line for your OS kernel.

Once successfully executed, you should have a ZIP-archived OS package file and the corresponding JSON descriptor file in your current working directory. Both of these needs to be stored in the same directory/kept together for the signing process.

The next step is to sign the package with your keys. See [this page](./ospkg_keys.md) in case you don't have any keys yet.
```bash
stmgr ospkg sign -ospkg path/to/your-os-pkg.zip \
-key=path/to/your-signing.key \
-cert=path/to/your-signing.cert 
```
Depending on your [Trust Policy](/docs/reference/trust_policy.md) you may want to repeat this multiple times with different keys.

Run `stmgr ospkg -help` for a complete list of options.

At this point your OS package is ready to be placed at one of supported locations for stboot to fetch and boot it.
