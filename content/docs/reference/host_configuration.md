---
title: Host configuration
weight: 9
---

# Host configuration

A host configuration is a collection of host-specific settings.  Examples of
such settings include static IP address, network prefix, and default gateway.

## Settings

The host configuration settings are encoded as key-value pairs in JSON format.
Parsing of a key-value pair that is omitted or explicitly set to "null" is
treated in the exact same way.

| Field              | Type         | Required                         | Short description                                       |
|--------------------|--------------|----------------------------------|---------------------------------------------------------|
| ospkg_pointer      | string       | yes                              | Location of the OS package (URLs or relative file name) |
| description        | string       | no                               | Optional information, e.g., provisioning date           |
| network_mode       | string       | yes                              | Static or DHCP-based IP address assignment              |
| host_ip            | string       | if network_mode is "static"      | Static IP address and network prefix to configure       |
| gateway            | string       | if network_mode is "static"      | IP address of the default gateway                       |
| dns                | string array | no                               | One or more DNS server IP addresses to configure        |
| network_interfaces | object array | no                               | One or more network interfaces to consider configuring  |
| bonding_mode       | string       | no                               | Bonding mode to use for the specified interfaces        |
| bond_name          | string       | if bonding_mode is set           | Name of the logical interface to bond interfaces into   |

### **ospkg_pointer**

The format of the OS package pointer is determined by a separate [trust
policy](./trust_policy.md) configuration.

If `trust_policy.ospkg_fetch_method` is set to "network", then the format is a
comma-separated list of HTTP(S) URLs used for downloading an OS package
descriptor.  List these URLs in order of preference.

If `trust_policy.ospkg_fetch_method` is set to "initramfs", then the format is a
relative file name specifying where an OS package resides in the initramfs's
"/ospkg/" directory.  For example, if the OS package archive and its descriptor
are located at "/ospkg/ospkg.{zip,json}", then "ospkg.json" would be a valid OS
package pointer.  Any suffix like ".json" and ".foo" will be correctly
substituted.

### **description**

When present, this field contains a human readable, and reasonably
short, description of this host config. It is logged to the console
during boot (after removing or replacing any control characters). An
organization's provisioning tools (e.g., stprov) and fleet management
tools may apply additional conventions for how this field is used,
e.g., including a provisioning timestamp and the provisioning software
version.

### **network_mode**

There are two supported network modes: static IP address assignment and dynamic
IP address assignment using the DHCP protocol.  Specify "static" or "dhcp" to
select between the two different network modes.

### **host_ip**

Static IP address and network prefix to be configured in CIDR notation.  For
example, "10.0.2.15/25" or "2001:db8::15/64".

### **gateway**

IP address of the default gateway to configure in static network mode.  For
example, "10.0.2.1" or "2001:db8::1".

### **dns**

A list of DNS server IP addresses to use for resolving domain names.  List these
DNS servers in order of preference.  For example, `["10.0.2.2", "2001:db8::2"]`.

If this field is omitted, DNS resolution may not work.

### **network_interface**

A list of network interfaces that should be considered for configuration.  Each
network interface is identified by its interface name and MAC address.   For
example, `[{"interface_name": "eth1", "mac_address": "bb:bb:bb:bb:bb:bb"}]`.

If bonding is enabled, the specified network interfaces should be bonded as a
single logical interface.  Otherwise a network interface should be selected,
preferring the first network interface that is listed and considered to work.

If this field is omitted, implementers should fallback on a guessed network
interface.

### **bonding_mode**

The following bonding mode values can be parsed:

  - "balance-rr"
  - "active-backup"
  - "balance-xor"
  - "broadcast"
  - "802.3ad"
  - "balance-tlb"
  - "balance-alb"

It is up to implementers to determine which of these modes are supported.

### **bond_name**

Name of the logical interface to bond the specified interfaces into (arbitrary
string).

## Examples

### initramfs configuration

```json
{
    "ospkg_pointer": "ospkg.json",
    "network_mode": "dhcp"
}
```

### DHCP network configuration

```json
{
    "ospkg_pointer": "https://example.org/ospkg.json",
    "network_mode": "dhcp",
    "dns": [
        "9.9.9.9",
    ],
    "network_interfaces": [
        {
            "interface_name": "eth0",
            "mac_address": "aa:aa:aa:aa:aa:aa",
        }
    ]
}
```

### Static network configuration with bonding

```json
{
    "ospkg_pointer": "https://ospkg-01.example.org/ospkg.json,https://ospkg-02.example.org/ospkg.json",
    "network_mode": "static",
    "dns": [
        "9.9.9.9",
        "149.112.112.112",
    ],
    "network_interfaces": [
        {
            "interface_name": "eth1",
            "mac_address": "bb:bb:bb:bb:bb:bb",
        },
        {
            "interface_name": "eth2",
            "mac_address": "cc:cc:cc:cc:cc:cc",
        }
    ],
    "bonding_mode": "802.3ad",
    "bond_name": "bond0"
}
```
