#!/bin/bash

#
# A script that assembles docs.system-transparency.org/vX.Y.Z.  Mainly meant to
# work in the context of a collection release, i.e., ../ contains components.
#
#   Usage: ./assemble.sh
#
# If you want to use this script outside the context of a collection release
# and, e.g., have all components in ../../core, try something like the below.
#
#   Usage: TARBALL_ROOT=../../core MANIFEST=$TARBALL_ROOT/system-transparency/collection-releases/st-1.0.0.manifest ./assemble.sh
#
# In other words, the above would render docs based on the locally checked-out
# components in ../../core and the placeholder manifest file v1.0.0.manifest.
#

set -eu
cd "$(dirname "$0")"

# Where the collection tarball has its root relative to this script
TARBALL_ROOT=${TARBALL_ROOT:-..}

# Manifest file describing collection and component versions, see format:
# https://git.glasklar.is/system-transparency/project/docs/-/blob/main/content/docs/releases/release-manifest.md
MANIFEST=${MANIFEST:-$TARBALL_ROOT/manifest}

# The hugo baseURL of docs.sto.  It will get an ST collection version appeneded
# to it, e.g., to become "https://docs.system-transparency.org/v1.0.0/".
URL=${URL:-https://docs.system-transparency.org}

# Reference docs that are pulled-in from the collection's components.  The first
# filename is where to copy to.  The second filename is where to copy from.
# Note that the destination file must exist and already contain hugo metadata.
REFDOCS=(
    "./content/docs/reference/stboot-system.md $TARBALL_ROOT/stboot/docs/stboot-system.md"
    "./content/docs/reference/stprov-system.md $TARBALL_ROOT/stprov/docs/stprov-system.md"
    "./content/docs/reference/stprov-manual.md $TARBALL_ROOT/stprov/docs/stprov-manual.md"
    "./content/docs/reference/stmgr-manual.md  $TARBALL_ROOT/stmgr/docs/manual.md"
)

# Markdown links that contain the first two patterns are replaced with the local
# filename listed as the final argument.  A markdown link is only replaced if it
# is formatted as "[Example][]" and then defined as "[Example]: URL" later.  If
# the URL does not start with https://git.gtis/st, it will be skipped.  If the
# URL contains a section at the end (#some-section), it will be kept intact.
#
# Note: these replacements ensure we have coherent linking within a collection.
# We don't touch local links.  We also don't touch links on the format []().
REFDOCS_REPLACEMENTS=(
    "project/docs os_package.md         ./os_package.md"
    "project/docs host_configuration.md ./host_configuration.md"
    "project/docs trust_policy.md       ./trust_policy.md"
    "project/docs efi-variables.md      ./efi-variables.md"
    "core/stboot  stboot-system.md      ./stboot-system.md"
    "core/stprov  stprov-system.md      ./stprov-system.md"
    "core/stprov  stprov-manual.md      ./stprov-manual.md"
    "core/stmgr   manual.md             ./stmgr-manual.md"
)

# Location where to generate a markdown table from the manifest.  This file must
# already exist with appropriate hugo metadata (such as title and weight).
COMPONENTS_DST=./content/docs/introduction/components.md

function info() {
    echo "INFO: $*" >&2
}

#####
# Configure config.toml with a versioned baseURL
#####
version=$(grep 'collection:' "$MANIFEST" | cut -d' ' -f2)
url=$URL/$version/
sed -i "s|^\(baseURL\s*=\s*\).*|\1\"${url}\"|" config.toml
info "config.toml: set baseURL = $url"

#####
# Generate components.md from the manifest file
#####
dst=$COMPONENTS_DST
awk '/^---$/ {count++; if (count == 2) {print; exit} } count < 2' "$dst" > "$dst.tmp"
cat <<EOF >>"$dst.tmp"
# Components

The documentation on this site is rendered for ST collection-release $version.
The included components and their versions are listed below.  Refer to the
respective repository URLs and the [reference section](../reference/_index.md)
for details on each component.  To learn more about what a collection and
component release is, see the [release section](../releases/_index.md).

| Component | git-tag | git-commit                               | URL |
| --------- | ------- | ---------------------------------------- | --- |
EOF
while IFS= read -r line
do
    if [[ $line != \#* && $line == component:* ]]; then
        read -r _ url git_tag git_commit <<< "$line"
        url=${url%.git}                   # Remove '.git' extension from URL
        component_name=$(basename "$url") # Extract component name from URL
        printf "| %-9s | %-7s | %s | %s |\n" "$component_name" "$git_tag" "$git_commit" "$url" >> "$dst.tmp"
    fi
done < "$MANIFEST"
mv "$dst.tmp" "$dst"
info "generated $dst from $MANIFEST"

#####
# Setup reference/ section by pulling in documentation
#####
for files in "${REFDOCS[@]}"; do
    read -r dst src <<< "$files"
    awk '/^---$/ {count++; if (count == 2) {print; exit} } count < 2' "$dst" > "$dst.tmp"
    cat "$src" >> "$dst.tmp"
    mv "$dst.tmp" "$dst"
    info "references: add $dst"
done

#####
# Replace git.gtis/st links with local files on docs.sto
#####
for filename in content/docs/reference/*.md; do
    awk -v str="${REFDOCS_REPLACEMENTS[*]}" '
    BEGIN {
        n = split(str, splits, /[[:space:]]+/);
        for (i = 1; i <= n; i += 3) {
            replacements[splits[i] " " splits[i+1]] = splits[i+2];
        }
    }

    function matchesAllPatterns(url, repository, filename) {
        return (url ~ repository && url ~ filename);
    }

    {
        if ($0 ~ /^\[.*\]: https:\/\/git\.glasklar\.is\/system-transparency/) {
            for (key in replacements) {
                split(key, patterns, " ");
                if (matchesAllPatterns($0, patterns[1], patterns[2])) {
                    fragment = "";
                    if (match($0, /#[^ ]+/, arr)) {
                        fragment = arr[0];
                    }
                    sub(/https:\/\/[^ ]+/, replacements[key] fragment);
                    break;
                }
            }
        }
        print $0;
    }' "$filename" > "$filename.tmp"
    diff -q "$filename" "$filename.tmp" >/dev/null || info "references: modify ./$filename (set local links)"
    mv "$filename.tmp" "$filename"
done

info "OK"
