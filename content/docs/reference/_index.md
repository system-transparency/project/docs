---
title: Reference
weight: 3
bookCollapseSection: true
---

# Reference

This section contains reference documentation for the current ST components:
[stprov][], [stmgr][], and [stboot][].  Example tooling that uses these
components to boot an OS package is provided in the [build guide][].

[stprov]: https://git.glasklar.is/system-transparency/core/stprov
[stmgr]: https://git.glasklar.is/system-transparency/core/stmgr
[stboot]: https://git.glasklar.is/system-transparency/core/stboot
[build guide]: ../introduction/build.md

## Overview

An overview of how the different ST components interact with each other is shown
in the figure below.  Click on the rendered components or use the left-side
submenu for navigating into the more detailed documentation.

At a glance, a provisioning tool (stprov) writes various configuration files to
a new platform.  Provisioning of a host configuration is required.  Additional
configuration that is useful for OS packages can optionally be provisioned using
an interactive client-server protocol.

A tool named stmgr is used to assemble various inputs that the ST bootloader
(stboot) reads.  For example, it can generate keys, sign OS packages, and
assemble stprov/stboot images suitable for running on a platform.

The ST bootloader (stboot) verifies OS packages against its trust policy.  If
all goes well, control is handed over to the verified OS package.  The OS
package may read some of the configuration provisioned by stprov.

```mermaid
flowchart TB
    %% For basic mermaid sytax, see:
    %% https://mermaid.js.org/syntax/flowchart.html
    subgraph Installation and boot
        %%%%
        %% Components (diamonds, red)
        %%%%
        stboot{{stboot}};               style stboot        fill:#FC2B59,stroke:#333,stroke-width:2px,color:white
        stmgr{{stmgr}};                 style stmgr         fill:#FC2B59,stroke:#333,stroke-width:2px,color:white
        stprov_client{{stprov local}};  style stprov_client fill:#FC2B59,stroke:#333,stroke-width:2px,color:white
        stprov_server{{stprov remote}}; style stprov_server fill:#FC2B59,stroke:#333,stroke-width:2px,color:white

        click stboot href "./stboot-system" "stboot system documentation"
        click stprov_client href "./stprov-manual" "stprov manual"
        click stprov_server href "./stprov-manual" "stprov manual"
        click stmgr href "./stmgr-manual" "stmgr manual"

        %%%%
        %% Formats (rounded rectangles, blue)
        %%%%
        hostconf([Host configuration]); style hostconf fill:#0C6695,stroke:#333,stroke-width:2px,color:white
        trustpol([Trust policy]);       style trustpol fill:#0C6695,stroke:#333,stroke-width:2px,color:white
        ospkg([OS package]);            style ospkg    fill:#0C6695,stroke:#333,stroke-width:2px,color:white
        ssh_key([SSH hostkey]);         style ssh_key  fill:#0C6695,stroke:#333,stroke-width:2px,color:white
        hostname([Hostname]);           style hostname fill:#0C6695,stroke:#333,stroke-width:2px,color:white

        click hostconf href "./host_configuration" "Host configuration"
        click trustpol href "./trust_policy" "Trust policy"
        click ospkg href "./os_package" "OS package"
        click ssh_key "./efi-variables" "Storage index - SSH hostkey"
        click hostname "./efi-variables" "Storage index - Hostname"

        %%%%
        %% Protocols (trapezoid, green)
        %%%%
        stprov_protocol[/stprov protocol/]; style stprov_protocol fill:#0c970c,stroke:#333,stroke-width:2px,color:white

        click stprov_protocol href "./stprov-system" "stprov system documentation"

        %%%%
        %% Edges
        %%%%
        stprov_server .-> hostname
        stprov_client <.-> stprov_protocol <.-> stprov_server
        stprov_server .-> ssh_key
        stprov_server --> hostconf --> stboot

        stmgr --> ospkg --> stboot
        trustpol --> stboot

        %% Consider depicting relevant inputs to stmgr?
    end

    %%%%
    %% Running OS package outside of the install+boot graph
    runtime>Running OS package]; style runtime fill:#ffff00,stroke:#333,stroke-width:2px

    %%%%
    %% Edges
    %%%%
    stboot x-- kexec syscall --x runtime
    hostconf .-> runtime
    hostname .-> runtime
    ssh_key .-> runtime
```

Diamonds (red) depict components used by users or scripts via command-line
interfaces and configuration files.  Rounded rectangles (blue) depicts formats
typically stored at rest.  Trapezoids (green) depicts protocols.  An outgoing
arrows indicates something is produced.  An incoming arrow indicates something
is consumed.

Note that it should be possible to replace a component if compatible formats and
protocols are used.  For example, where an OS package reads its hostname is up
to the user-defined OS package.  Similarly, there is no requirement on using
stprov if alternative tools can provision a compatible host configuration for
stboot.
