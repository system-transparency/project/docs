---
title: Signature verification
weight: 3
---
# Signature verification

The System Transparency release keys are maintained at
https://git.glasklar.is/system-transparency/core/system-transparency/-/tree/main/keys,
and published at
https://www.system-transparency.org/keys.

To verify release signatures, an authentic copy of the file
`allowed-ST-release-signers` is needed.

## Verifying git release tags

To verify a release git tag, e.g., vX.Y.Z, use
```
git -c gpg.format=ssh -c gpg.ssh.allowedSignersFile=allowed-ST-release-signers tag --verify vX.Y.Z
```

If desired, the settings can be made more permanent using `git config`.

## Verifying release archives and other artifacts

To verify the signature on a release artifact, e.g, `foo.tar.gz` with
signature `foo.tar.gz.sig`, use
```
ssh-keygen -Y verify -f allowed-ST-release-signers -I releases@system-transparency.org -n file -s foo.tar.gz.sig  < foo.tar.gz
```

## Signatures on the `allowed-ST-release-signers` file

The `allowed-ST-release-signers` file is self-signed with each listed key
(except temporarily, see Updates below). E.g., the self signature by
key-holder nisse can be verified using
```
ssh-keygen -Y verify -f allowed-ST-release-signers -I nisse@glasklarteknik.se -n file -s allowed-ST-release-signers.nisse.sshsig < allowed-ST-release-signers
```

The file may also be signed using OpenPGP. Files
`allowed-ST-release-signers.*.pgpsig` should be valid detached OpenPGP
signatures, which can be verified with GnuPG (provided that the needed public key
is available) using
```
gpg --verify allowed-ST-release-signers.user.pgpsig allowed-ST-release-signers
```

## Updates

When the allowed signers file is updated, the person updating the file
is expected to sign it, and remove previous signatures that don't
apply to the updated version. That way, there's always at least one
self-signature, and there should never be any signatures that fail to
verify. (The older signatures stay available in git history, together
with the version of the allowed signers file they apply to). Key
holders are expected to coordinate, so that they can all sign the
updated file and add their new self-signatures in a timely manner.
