---
title: enrollv0
math: true
---

# enrollv0

enrollv0 defines the protocol messages used to enroll a device into the
attestation system. During enrollment a key hierarchy is established in the
device's TPM, firmware checksums are extracted and sent back to the operator.

The enrollment protocol only defines messages between the operator's machine
and the device to be attested. The description of the protocol flow includes
the device-to-TPM communication to make it easier to understand.

For detailed definitions of the TPM commands and structures used in this
document, refer to parts two and three of the [Trusted Platform Module Library
Specification](https://trustedcomputinggroup.org/resource/tpm-library-specification/)
published by the Trusted Computing Group.

```mermaid
sequenceDiagram
    autonumber
    participant O as stauth on Operator
    participant D as stauth on Device
    participant TPM
    
    TPM->>D: EK, SRK, AIK

    D->>O: EK, SRK, AIK

    O->>D: Challenge = E(EK, Nonce), AIK
    activate D

    D->>TPM: TPM2_ActivateCredential(EK, AIK, Challenge)
    TPM->>D: Nonce

    D->>O: Nonce, Event Log
    deactivate D
```

## Step 1: Key Generation

The purpose of the enrollv0 protocol is to certify keys generated in the
device's TPM. The first step is to generate them. Later they will be used to
sign the attestation structure sent in the quotev0 protocol.

The two keys generated are a Storage Root Key (SRK) and an Attestation Identity Key
(AIK). The SRK act as a Primary Object in the TPM 2.0 key hierarchy under which
the AIK is created.

The SRK is created using TPM2_CreatePrimary under the Storage Hierarchy. The
AuthValue is set to the empty string and the public parameters are listed in
the table below. In short the SRK is a ECDH + AES-128/CFB hybrid encryption
key.

| Field                                    | Value                                                                               |
|------------------------------------------|-------------------------------------------------------------------------------------|
| type                                     | TPM_ALG_ECC                                                                         |
| nameAlg                                  | TPM_ALG_SHA256                                                                      |
| objectAttributes                         | fixedTPM, fixedParent, sensitiveDataOrigin, userWithAuth, noDA, restricted, decrypt |
| authPolicy                               | *Empty Buffer*                                                                      |
| parameters.eccDetail.symmetric.algorithm | TPM_ALG_AES                                                                         |
| parameters.eccDetail.symmetric.keyBits   | 128                                                                                 |
| parameters.eccDetail.symmetric.mode      | TPM_ALG_CFB                                                                         |
| parameters.eccDetail.scheme.scheme       | TPM_ALG_NULL                                                                        |
| parameters.eccDetail.curveID             | TPM_ECC_NIST_P256                                                                   |
| parameters.eccDetail.kdf.scheme          | TPM_ALG_NULL                                                                        |
| unique                                   | *Empty Buffer*                                                                      |

The AIK is created is an Ordinary Object under the SRK using
TPM2_Create. The AuthValue is the empty string and it's public parameters are
listed in the table below. The AIK is a restricted ECDSA signing key.

| Field                                    | Value                                                                     |
|------------------------------------------|---------------------------------------------------------------------------|
| type                                     | TPM_ALG_ECC                                                               |
| nameAlg                                  | TPM_ALG_SHA256                                                            |
| objectAttributes                         | fixedTPM, fixedParent, sensitiveDataOrigin, userWithAuth, restricted, ign |
| authPolicy                               | *Empty Buffer*                                                            |
| parameters.eccDetail.symmetric.algorithm | TPM_ALG_NULL
| parameters.eccDetail.scheme.scheme       | TPM_ALG_ECDSA
| parameters.eccDetail.scheme.hashAlg      | TPM_ALG_SHA256
| parameters.eccDetail.scheme.count        | 0
| parameters.eccDetail.curveID             | TPM_ECC_NIST_P256                                                         |
| parameters.eccDetail.kdf.scheme          | TPM_ALG_NULL                                                              |
| unique                                   | *Empty Buffer*                                                            |

The SRK is regenerated each time it's needed while the AIK is stored encrypted
on the operators computer and sent each time it's needed.

## Step 2: Request Message

The device sends the AIK (quote signing key), SRK (AIK's parent key) and the EK
(TPM identifier). The SRK i.e. Storage Root Key is a primary key that encrypts
the AIK when stored outside the TPM. The SRK is not randomized, recreated each
time it's needed and differs only between TPMs. The AIK is a random signing key
that is generated inside the TPM and cannot be exported in plaintext. Finally,
the EK is a unique key. It and it's accompanying certificate identify a TPM as
genuine. The Request message is the protobuf structure described briefly below.
The complete definitions can be found in the [stauth
repository](https://git.glasklar.is/system-transparency/core/stauth/-/blob/f84a97b2397f6b858fac6fbfd721b4fa143cb1eb/enrollv0/enrollv0.proto).
It is sent from the device to the operator with a HTTP GET from
`/enrollv0/request` and content type `application/protobuf;
proto=enrollv0.Request`.

| Field            | Contents                                                                |
|------------------|-------------------------------------------------------------------------|
| `ek_certificate` | Optional DER encoded X.509 Endorsement certificate.                     |
| `ek_public`      | `TPM2B_PUBLIC` structure of the Endorsement key.                        |
| `srk_public`     | `TPM2B_PUBLIC` structure of the stauth-specific Storage key.            |
| `aik_public`     | `TPM2B_PUBLIC` structure of the Attestation Identity key.               |
| `aik_private`    | `TPM2B_PRIVATE` structure of the Attestation Identity key.              |
| `ux_identity`    | Human-readable device identity string. Trailing zero bytes are trimmed. |

## Step 3: Challenge Message

The operator's machine responds with a challenge $p$ encrypted with the EK using
the TPM2\_MakeCredential protocol $\mathrm{Make}$. To make the protocol
stateless on the device end the operator sends the AIK it received as part of
the Request message back. The detailed encryption operations are show in
equation (1).

$$
\begin{equation}
\begin{split}
              p &\in\_{R} \\{0,1\\}^{256} \\\\
              k &\in\_{R} \\{0,1\\}^{128} \\\\
    \mathit{iv} &\in\_{R} \\{0,1\\}^{96} \\\\
              c &= E_{k,\mathit{iv}}(p) \\\\
          (a,b) &\leftarrow \mathrm{Make}(\mathit{EK}, k, \mathrm{Name}(\mathit{AIK}))
\end{split}
\end{equation}
$$

The operator chooses a random 32 byte challenge $p$, a 16 byte AES key $k$ and
a 12 byte GCM initialization vector $\mathit{iv}$. The challenge is encrypted
using AES-128/GCM and the encryption key is protected using the TPM 2.0
$\mathrm{Make}$ function. The values $(\mathit{iv}, E(p), a, b)$ is sent to the
device in the protobuf encoded Challenge message described briefly below. The
complete definitions can be found in the [stauth
repository](https://git.glasklar.is/system-transparency/core/stauth/-/blob/f84a97b2397f6b858fac6fbfd721b4fa143cb1eb/enrollv0/enrollv0.proto).

| Field                  | Contents                                                                   |
|------------------------|----------------------------------------------------------------------------|
| `aik_public`           | `TPM2B_PUBLIC` structure of the Attestation Identity key.                  |
| `aik_private`          | `TPM2B_PRIVATE` structure of the Attestation Identity key.                 |
| `encrypted_credential` | The encrypted AES-128/GCM key $a$.                                         |
| `encrypted_secret`     | The EK public key encrypted seed $b$, protecting the credential value $a$. |
| `ciphertext`           | AES-128/GCM encrypted random challenge $c$.                                |
| `nonce`                | Random IV $iv$ for $c$.                                                    |

The Challenge message is sent via HTTP POST to `/enrollv0/activate` with
content type `application/protobuf; proto=enrollv0.Challenge`.

## Steps 4 & 5: Credential Activation

The device will recreate the SRK, load the AIK and use the
TPM2\_ActivateCredential TPM command $\mathrm{Activate}$ the decrypt the
challenge encryption key $\mathit{k}$. $\mathrm{Activate}$ will only decrypt
$\mathit{k}$ if the AIK is loaded. This ensures the challenge cannot be decrypted
if the AIK is loaded into a different TPM (or simulator).

$$
\begin{equation}
\begin{split}
    k &= \mathrm{Activate}(\mathit{AIK}, \mathit{EK}, a, b) \\\\
    p &= D\_{k,\mathit{iv}}(c)
\end{split}
\end{equation}
$$

## Step 6: Answer Message

The device sends back the decrypted challenge and the TPM event log. After
receiving the Answer message the operator confirms that the decrypted challenge
is equal to the $p$ sent in step 3. If yes the event logs are concatenated and
used to produce the platform endorsement structure described in `endorsev0`.

The Answer message is sent as response to the HTTP POST request in step 3 with
content type `application/protobuf; proto=enrollv0.Answer`. The message
structure is explained below in brief. The complete definitions can be found in
the [stauth
repository](https://git.glasklar.is/system-transparency/core/stauth/-/blob/f84a97b2397f6b858fac6fbfd721b4fa143cb1eb/enrollv0/enrollv0.proto).

| Field              | Contents                                  |
|--------------------|-------------------------------------------|
| `nonce`            | The decrypted challenge $p$.              |
| `uefi_event_log`   | The UEFI TPM 2.0 event log [^1].          |
| `stboot_event_log` | The TPM 2.0 event log produced by stboot. |

After the platform endorsement structure has been generated the enrollv0
protocol concludes.

# References

[^1]: The event log format is described in section 10 of the TCG PC Client
      Platform Firmware Profile Specification
