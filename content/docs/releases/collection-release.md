---
title: Collection release
weight: 2
---

# Collection release

The System Transparency project releases collections of components that
are documented and tested to work together.  A _collection_ is defined
by a manifest-file listing included components and their versions.  The
release artifact is a tar archive containing the manifest file, the
source code of each included component, a NEWS file summarizing what
changed, and software versions related to tar-archive reproducibility.

If you are looking to depend on System Transparency in production
environments, use of collection releases within the same major version
provides the greatest stability guarantees.  See semantic versioning
below.

## Semantic versioning

Collection releases are semantically versioned and formatted as
`st-MAJOR.MINOR.PATCH`.

  - **MAJOR:** Increased on large and breaking changes.
  - **MINOR:** Increased for additions and changes that are meant to be
    backwards compatible.  In other words, upgrading from `st-1.0.0` to
    `st-1.1.0` is expected to work without issues.  The same is not
    necessarily true on downgrades from `st-1.1.0` to `st-1.0.0`.
  - **PATCH:** Increased for changes that are meant to be both backwards
    and forward compatible.  In other words, it should be possible to
    switch back and forth between `st-1.0.0` and `st-1.0.1`.

The current collection release series is at version 1.  It is supported
until at least 2025-06-01.  This date may be extended on request.
Additional collection release series may be added and maintained in
parallel.

## Distribution and documentation

Collection releases can be downloaded as tar archives from
https://dist.system-transparency.org/.  These tar archives are signed by
one of the [release keys][].  The documentation of
`st-MAJOR.MINOR.PATCH` is available at
`https://docs.system-transparency.org/st-MAJOR.MINOR.PATCH/`.  You may [build
the documentation][] locally from the unpacked tar archive.  This requires an
installation of [hugo][] but not an internet connection.

To be notified when new collection releases are available, subscribe to
the [st-announce][] mailing list.

[release keys]: https://www.system-transparency.org/keys
[build the documentation]: https://git.glasklar.is/system-transparency/project/docs#build-and-deploy-the-site-for-a-collection-release
[core/system-transparency]: https://git.glasklar.is/system-transparency/core/system-transparency/
[hugo]: https://gohugo.io/
[ST-announce]: https://lists.system-transparency.org/mailman3/postorius/lists/st-announce.lists.system-transparency.org/

## Release artifact

The release artifact is a tar archive (`.tar.gz`).  The structure of the
archive's content is shown below.

    ├── archiving-tools.txt
    ├── manifest
    ├── NEWS
    ├── docs/
    ├── stboot/
    ├── stmgr/
    ├── stprov/
    └── ...

`archiving-tools.txt` lists relevant software versions which were used
to create the tar archive.  This format is adhoc and subject to change.
It should however be sufficient to [reproduce the tar archive][]
provided the same archiving tools and manifest is used.

`manifest` is a file specifying the collection release version and the
included components.  See formatting below.

`NEWS` summarizes what changed since the previous collection release,
aggregated for all components that are included.  It should be
sufficient to digest this file in order to upgrade without issues.

`docs/`, `stboot/`, `stmgr/`, `stprov/`, `...` are the components listed
in the manifest.  These subdirectories include source code without any
dependencies.  This means an internet connection is required to build
the software.

Note: all Go dependencies in the software are fixated by go.mod and
go.sum files.  In other words, one can be sure that the same
dependencies are downloaded each time the above software is built.

[reproduce the tar archive]: https://git.glasklar.is/system-transparency/core/system-transparency/-/blob/main/releng/mk-release-archive.sh

## Manifest file

A manifest is a text file with the following syntax:

    collection: <string>
    component: <git-repo> <tag> <git-hash>
    ...

Empty lines, and lines starting with the `#` character, are ignored.
The values provided on each line are white-space separated (and hence,
values must not include any spaces).

The `collection:` line specifies the name and version of the
collection, e.g., `st-1.0.0`.

Each `component:` line specifies one component that is part of the
collection. The first argument is a git repository, e.g.,
`https://git.glasklar.is/system-transparency/core/stboot.git`. The
second argument is a tag in that repository, typically including a
version number. The tag should be signed by an appropriate release
key. The final argument is the git hash of the tagged commit,
identifying the contents of the component. To fetch a listed
component, one should

* checkout the repository at the given tag,
* verify the tag signature,
* verify that the HEAD commit hash equals the one in the manifest.
