# docs.system-transparency.org

This website is built using [Hugo][] and the (forked) [Hugo Book][] theme.

[Hugo]: https://gohugo.io/
[Hugo Book]: https://git.glasklar.is/system-transparency/project/documentation-theme

## Prerequisites

`hugo` is available on most distributions, see [install instructions][].  For
example, on a Debian-based system it should work to run `apt install hugo`.

After cloning this repository, configure the theme submodule:

    $ git submodule update --init --recursive

You don't need to do this if you downloaded and unpacked a collection release.

[install instructions]: https://gohugo.io/installation/

## Build and deploy the site for a collection release

Building the site for deployment at `docs.system-transparency.org` involves use
of the [assemble.sh][] script.  This script copies reference documentation from
other repositories so that it can be rendered here.  The script also fixes HTTPS
links and a few other files like `config.toml`.

[assemble.sh]: ./assemble.sh

In the context of a collection release where [../
contains](./content/docs/releases/collection-release.md#release-artifact) a
manifest file and the components to copy reference documentation from, build the
site as follows:

    $ ./assemble.sh
    $ hugo

Output is written to the `public` directory.  Transfer it to the web server as a
directory named `st-X.Y.X` if the collection version is `st-X.Y.X`.

## Build the site without a collection release

See the [assemble.sh][] script for details on how to possibly use it during
development, e.g., for local preview before cutting a collection release.

**Be warned:** files will be modified as a result of running the script.

## Browse the site locally

    $ hugo serve

The above renders the site at http://localhost:1313.

**Note:** the site can be rendered without first using [assemble.sh][].  Some of
the copied and generated pages will just be blank then.  This is fine for most
of the editing one would do on content maintained in this repository.

## Updating the Hugo theme

If a new version of the (forked) [Hugo book][] theme becomes available, run:

    $ (cd themes/book && git checkout st && git pull origin st)
    $ git add themes/book

Expect a one-line diff on the subproject commit-id when committing the changes.

If someone upgraded the Hugo theme and your local submodule is behind, run:

    $ git submodule update
