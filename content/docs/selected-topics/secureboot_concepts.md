---
title: UEFI Secure Boot
weight: 1
---

# UEFI Secure Boot

This document gives an introduction to UEFI Secure Boot and the possible ways of
executing the stboot bootloader as part of the UEFI handoff to the bootloader.

## What is UEFI?

The Unified Extensible Firmware Interface (UEFI) specification was invented around 1998 by the Intel Corporation and kept confidential until 2006. In 2012, the "Secure Boot" feature got introduced to the specification. The specification main goal is standardization among the firmware ecosystem.

What nowadays users see as UEFI firmware is mostly the UI and the bootloader interface called "BDS" which loads a special crafted PE32 executable after firmware handoff to the bootloader. The most famous UEFI firmware implementation is the [TianoCore project](https://www.tianocore.org).

## How does UEFI Secure Boot work?

The visualization shows how the UEFI boot sequence starts from power on to shutdown.

{{< img src="PI_Boot_Phases.JPG" >}}

### Security (SEC)

The SEC stage represents the initial code execute as part of the UEFI firmware. It initializes the basics like Cache-As-RAM, serial console, cpu register and more. This stage is also known for start further verification of the later stages. 

*Don't be mistaken UEFI Secure Boot doesn't start here nor it protects this component.*

### Pre-EFI Initialization Environment (PEI)

The PEI stage mainly initializes the DRAM and configures parts of the platform.

*Don't be mistaken UEFI Secure Boot doesn't start here nor it protects this component.*

### Driver Execution Environment (DXE)

The DXE stage is the biggest one in the UEFI ecosystem. PCI Device Tree enumeration, Device Drivers, ACPI, SMBIOS, Graphics and more is initialized here. It also contains all drivers for accessing filesystem, network and doing cryptography. The stage will also configure/enforce UEFI Secure Boot and load external code called option roms.

*UEFI Secure Boot starts here not at the beginning but somewhere in between as part of the dependency tree. It protects the BDS stage, option roms and everything after the BDS stage.*

### Boot Device Selection (BDS)

This stage tries to locate and execute all bootloader application as part of the boot order.

*UEFI Secure Boot protects here the execution of the bootloader.*

### PKI

UEFI Secure Boot comes with a lot of key types. It also is often pre-provisioned by the hardware vendor with multiple keys instead of one to allow booting different operating systems and creating something like the [web of trust model in firmware.](https://en.wikipedia.org/wiki/Web_of_trust)

| Type                               | Description                                                  | Amount         | Verified By |                                   Update |
| :--------------------------------- | :----------------------------------------------------------- | -------------- | ----------: | ---------------------------------------: |
| Platform Key (PK)                  | Root of trust in the PKI. Used to verify KEKs and itself     | one per device |           - | yes when signed by PK or UEFI setup mode |
| Key Exchange Key (KEK)             | Used for verifying db and dbx. <br />Allows to load additional drivers and firmware updates | no limit       |          PK |                    yes when signed by PK |
| Allowed Signature Database (DB)    | Whitelist: A list of hashes or certificates to be allowed to execute | no limit       |        KEKs |                   yes when signed by KEK |
| Forbidden Signature Database (DBX) | Blacklist: A list of hashes or certificates to be forbidden to execute | no limit       |        KEKs |                   yes when signed by KEK |

### UEFI Secure Boot Execution

This diagram shows the execution flow with the decision tree from initialization till bootloader execution.

#### DXE

```mermaid
graph LR
A(DXE Invocation) --> B{SB Enabled?}
    B -->|Yes| C{PK verify KEK?}
    B -->|No| D(Boot any BL)
    C -->|Yes| E{KEK verify db/dbx?}
    C -->|No| F(Do not add to cert store)
    E -->|Yes| G(BDS Stage)
    E -->|No| H(Do not add to cert store)
```

#### BDS

```mermaid
graph LR
    G(BDS Stage) --> I{BL hash in dbx}
    I -->|Yes| J(Stop execution)
    I -->|No| K{BL Signed?}
    K -->|Yes| L{BL Signatue in dbx?}
    K -->|No| M(BL Hash in db?)
    L -->|Yes| J
    L -->|No| N{BL Signature in db?}
    M -->|Yes| O(Execute BL)
    M -->|No| P(Stop execution)
    N -->|Yes| O
    N -->|No| P(Stop execution)
```

## Why do we need Secure Boot?

UEFI Secure Boot comes with an ownership model which allows you to build your own root-of-trust beginning with the firmware by deploying keys via physical presence interface. In-short you are able to verify the bootloader which is loaded by the firmware in order to protect from [bootkits](https://en.wikipedia.org/wiki/Rootkit#bootkit).

1. On some systems, UEFI Secure Boot is required for doing TPM 2.0 measurements during startup. Since ST utilize the TPM 2.0, it is a requirement. The UEFI specification doesn't match the real machines firmware implementation. You can only know for sure when Secure Boot is enabled inside the UEFI firmware configuration by yourself.
2. The Secure Boot transits from the firmware level to the bootloader application as part of the firmware handoff procedure. Therefore Secure Boot is enabled on the most platforms nowadays, requiring to support it for Linux distributions and for ST as well. It became the de-facto standard nowadays on modern systems
3. UEFI Secure Boot reduces the attack surface of the UEFI firmware itself because it requires option roms (PCI hardware firmware) to be signed by the OEM/Microsoft. Without this protection mechanisms it is possible to attack the firmware itself by [providing a malicious option rom.](https://x86sec.com/posts/2022/09/26/uefi-oprom-bootkit/)

## Take aways before moving on

* Nowadays all systems comes with UEFI firmware which provides a mechanism called [Secure Boot](https://tianocore-docs.github.io/Understanding_UEFI_Secure_Boot_Chain/draft/secure_boot_chain_in_uefi/uefi_secure_boot.html). Under the link here provided there should be an extensive documentation about the mechanism protection and transitioning to the bootloader loaded after the end of the firmware.
* On some systems, UEFI Secure Boot is required for doing TPM2 measurements during startup.
* UEFI Secure Boot comes on the most systems with pre-provisioned keys from Microsoft and the OEM/ODM vendor. Those keys are set in stone and can only be revoked by manually provisioning (physical presence in the BIOS menu) new keys.
* There is an exception to manually provisioning on server systems offering the feature as paid BMC addon which comes with API access for easy provisioning.
* The most users tend to disable UEFI Secure Boot on server systems or using the standard signed [SHIM model](https://github.com/rhboot/shim) as part of common Linux distribution like Redhat, CentOS, Ubuntu and Debian. This enables them without changing the keys to boot their favourite Linux distribution.
* UEFI supports CRL but manages that [really really bad](https://www.welivesecurity.com/2023/03/01/blacklotus-uefi-bootkit-myth-confirmed/). It also comes with size constraints due to limited CRL storage inside firmware.

## ST Deployment Options

In this flow diagram you can see possible routes of integration:

```mermaid
graph LR
A(UEFI Secure Boot) --> B{LinuxBoot support?}
    B -->|Yes| C(Embed into firmware)
    B -->|No| D{Secure Boot enforced?}
    D -->|Yes| E{Factory Keys?}
    D -->|No| F(Unsigned Measured Boot)
    E -->|Yes| G(SHIM/MS Signing)
    E -->|No| H(Custom Keys)
    G --> I{MOK Support}
    I -->|Yes| J(Prebuilt & Signed Artifacts)
    I -->|No| K(Source Code only)
```

| Integration | Description | Pros | Cons |
| :-- | :-- | :-- | :-- |
| Embed into Firmware | In this scenario the stboot bootloader is integrated through BMC or in code as part of the host firmware image (aka coreboot). Since the SPI flash is write protected, no additional signing procedure might be required | No extra signing required, since part of the firmware. | Requires machines to support LinuxBoot or custom hardware. |
| Unsigned Measured Boot | In this scenario stboot is launched directly without the Secure Boot mechanism after UEFI finalization. | Easy and no further steps required. | Maybe no measured boot available depending on the platform implementation. UEFI doesn't define it well, so it's unclear if measured boot is active.<br/><br />Unsigned option ROM execution is possible and may lead to a greater UEFI firmware attack surface. |
| Custom Keys | In this scenario you provision your own keys into the UEFI firmware by clearing the factory keys:<br /><br />**1st Option:** Using firmware UI with physical presence <br /><br />**2nd Option:** Using BMC API which requires Redfish or IPMI support <br /><br />**3rd Option:** Systemd sd-boot, requires local storage and machine in setup mode | Scalable when done via BMC API. <br /><br />Removes OEM/Microsoft certificates from your threat model.<br /><br />No extra code in between UEFI and stboot. | Requires physical presence using the UI which may not work out for servers.<br/><br/>Increases complexity by the need of provisioning, revoking and maintaining credentials.<br /><br /> Need to include OEM keys for PCI option-roms being executed. |
| SHIM/MS Signing | In this scenario the project will provide a signed version of SHIM or signed stboot bootloader by Microsoft including a certificate of the project. | A lot of systems nowadays have UEFI secure boot enabled by default. This enables us to easily deploy stboot on them. This version without MOK support is only useful for the project itself. <br/><br /> | You live in the same threat model as Microsoft, the OEM/ODM and the SHIM guys when remote attestation isn't utilized. <br /><br />Comes with overhead and requirements for the signing process and maintenance (see. [SHIM](https://github.com/rhboot/shim-review) and [Microsoft](https://techcommunity.microsoft.com/t5/hardware-dev-center/updated-uefi-signing-requirements/ba-p/1062916)).<br /><br />Requires the project to sign the community member stboot bootloader so they can utilize it. This comes with a huge burden of liability and other problems. |
| MOK Support / Signed Artifacts | A MOK feature is added as part of stboot bootloader. This enables the community members to use prebuilt & signed stboot bootloader releases. Possible integration could be the SHIM MOK approach or [TPM based approach](https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/10).<br /><br />Requires "SHIM/MS Signing" | Community member can use SHIM built and signed by MS, and (must) use stboot releases built by Glasklar. They can deploy this on existing Secure Boot systems without having to enroll keys into firmware (so possibly without physical access).<br/><br/>Liability and ownership lays in the hand of the community user. | Requires depending of the approach additional hardware like a TPM 2.0<br/><br/>Add an additional layer of complexity & maintenance for the project. |
| MOK Support / Source Code Only | In this scenario there is no MOK support for the community.  The requirement of community user to build SHIM or let Microsoft sign their own version of stboot bootloader. Another option is the custom keys deployment mentioned above. | No need to provide any prebuilt & signed binaries at all | No community support of  prebuilt & signed binaries.<br/><br/>A lot of complexity to utilize ST in the community by signing everything them self. |

## Learn more

If you want more detailed information on UEFI Secure Boot, refer to the
following documentation:

  - [Tianocore EDk2 Official](https://tianocore-docs.github.io/Understanding_UEFI_Secure_Boot_Chain/draft/secure_boot_chain_in_uefi/uefi_secure_boot.html)
  - [Rodsbooks](https://www.rodsbooks.com/refind/index.html)

Moreover, the stboot bootloader itself comes as Unified Kernel Image package,
introduced by the [UAPI
group](https://uapi-group.org/specifications/specs/unified_kernel_image/).
