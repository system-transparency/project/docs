---
title: Trust policy
weight: 6
---

# Trust policy

A trust policy is a directory of files.  For example, trusted root keys and the
number of signatures needed to consider an OS package valid are defined in it.
The location of the trust policy is defined by the components that require it,
see the [stboot][] and [stprov][] system documentation.

[stboot]: https://git.glasklar.is/system-transparency/core/stboot/-/blob/v0.3.6/docs/stboot-system.md
[stprov]: https://git.glasklar.is/system-transparency/core/stprov/-/blob/v0.3.5/docs/stprov-system.md

## Overview

The following files are part of the trust policy directory:

  - [trust_policy.json](./trust_policy.md#general-settings):
    general settings in key-value format
  - [ospkg_signing_root.pem](./trust_policy.md#os-package-signing-keys):
    OS package signing keys
  - [tls_roots.pem](./trust_policy.md#tls-roots):
    TLS signing keys
  - [decryption_identities](./trust_policy.md#decryption-identities):
    OS package decryption keys

Each of these files, their formats, and meanings are described next.

## General settings

The general trust policy settings are expressed as key-value pairs in JSON
format.  These settings are stored in a file named `trust_policy.json`.

| Field                     | Type    | Required | Short description                              |
|---------------------------|---------|----------|------------------------------------------------|
| ospkg_signature_threshold | integer | yes      | Number of required OS package signatures       |
| ospkg_fetch_method        | string  | yes      | How to locate OS packages                      |

### **ospkg_signature_threshold**

The number of signatures needed to consider an OS package valid.  Each valid
signature needs to correspond to a distinct OS package signing key.

The OS package signature threshold must be larger than zero.

### **ospkg_fetch_method**

Determines how OS packages are located.  The following values are supported:

  - "initramfs": read the OS package from the initramfs
  - "network": read OS packages from a web server using HTTP(S)

The exact initramfs location or web server URL(s) are configured in a separate
host configuration, see the [OS package
pointer](./host_configuration.md#ospkg_pointer) setting for details.

### Example

```json
{
  "ospkg_signature_threshold": 2,
  "ospkg_fetch_method": "network"
}
```

## OS package signing keys

The OS package signing keys are organized into a hierarchy starting with a
single trusted root key.  The root key can be used to sign OS packages directly,
or to sign leaf keys that in turn sign OS packages.  Intermediate keys are not
supported, which means that the maximum certificate length is two.

The keys in the OS package signing hierarchy are encapsulated as X.509
certificates in PEM format.  The PEM header must use the label "CERTIFICATE".
For successful chain building, the root certificate must set the cA-bit in the
[basic constraints extension][].  The maximum path length is irrelevant because
intermediate certificates are anyway not supported.  If the [key-usage bits][]
are set (optional), the root certificate must contain the keyCertSign bit.

For a certificate chain to be valid, it must chain back to the trusted root
stored in `ospkg_signing_root.pem`.  All certificates also need to be unexpired,
as specified by the [NotBefore and NotAfter][] timestamps.

There is no revocation mechanism.  The only way to rotate keys is to provision a
new trust policy, or to rely on the system time and the configured certificate
expiry dates.  Another way to deal with churn (e.g., an employee that quits) is
to increase the `ospkg_signature_threshold` so no signer can act on their own.

Only Ed25519 keys and signatures are supported.  How to _sign a certificate in a
chain_ using Ed25519 is defined by [RFC 8410][].  Please note that _signing of
an OS package_ with a leaf key is different from signing a certificate.

[basic constraints extension]: https://www.rfc-editor.org/rfc/rfc5280.html#section-4.2.1.9
[key-usage bits]: https://datatracker.ietf.org/doc/html/rfc5280#section-4.2.1.3
[NotBefore and NotAfter]: https://www.rfc-editor.org/rfc/rfc5280.html#section-4.1
[RFC 8410]: https://www.rfc-editor.org/rfc/rfc8410

## TLS roots

The file `tls_roots.pem` contains one or more trusted X.509 root certificates in
PEM format.  These root certificates are used to establish secure TLS
connections while downloading OS packages over a network.

If the OS package web servers use Let's Encrypt, then their [root certificate][]
in PEM format could be downloaded and stored as `tls_roots.pem`.  If all of the
web's certificate authorities should be configured, files like
`ca-certificates.crt` from the [ca-certificates package on Debian][] could be
used instead.  Nothing prevents users from deploying their own self-signed
certificates to side-step the web's certificate authorities.

[root certificate]: https://letsencrypt.org/certificates/#root-certificates
[ca-certificates package on Debian]: https://packages.debian.org/bookworm/ca-certificates

## OS package decryption keys

The file `decryption_identities` is optional. When provided, it enables
mandatory decryption of both the OS package and its associated descriptor using
the [age][] tool, as an additional layer of transport encryption. Decryption
applies only to OS packages and descriptors that are fetched over the network.

When present, the file must contain one or more decryption keys (identities) in
the [AGE X25519 recipient format][] with one key per line. Empty lines and ones
starting with "#" are ignored. OS packages are only accepted if both files can
be decrypted successfully (this means that unencrypted files are rejected).

[age]: https://github.com/FiloSottile/age
[AGE X25519 recipient format]: https://github.com/C2SP/C2SP/blob/main/age.md#the-x25519-recipient-type
