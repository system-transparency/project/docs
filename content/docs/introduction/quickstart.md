---
title: Quickstart guide
weight: 3
---

# Quickstart guide

*The quickest way to boot a System Transparency OS package*

----

This guide shows how to try out System Transparency by using prebuilt
images for booting an OS package, both in an emulator and on real
hardware.

The following topics will be covered:
* Downloading a prebuilt boot loader
* Booting a prebuilt OS package in [QEMU][]
* Booting a prebuilt OS package on a server

NOTE: The prebuilt boot loader trusts only Glasklar's QA keys, so you
will not be able to boot your own OS packages. Only OS packages from
the [Glasklar ST archive for QA][] can be booted using this boot
loader.

If you would rather build your own transparent system, please see the
[ST build guide](build).

[Glasklar ST archive for QA]: https://st.glasklar.is/st/qa
[QEMU]: https://en.wikipedia.org/wiki/QEMU


## Prerequisites

- You need a system running Linux.

- If you want to boot the OS package in QEMU your system must be able
  to start a VM with internet access through DHCP.

- If you want to boot the OS package on real hardware you need an x86
  computer capable of UEFI booting, sitting in a network with DHCP and
  connected to the internet.


### Prepare for booting in an emulator

For booting in QEMU, the following packages need to be installed. This
example assumes Debian 12 (Bookworm).

    apt install qemu-system-x86 ovmf


## Download boot loader images

Download the boot loader image in ISO format, for QEMU.

    curl -o stboot.iso https://st.glasklar.is/st/qa/releases/st-1.1.0/qa-stboot-amd64.iso

Download the boot loader image in [UKI][] format, for booting real hardware.

    curl -o stboot.uki https://st.glasklar.is/st/qa/releases/st-1.1.0/qa-stboot-amd64.uki

<!-- If you want to verify the downloaded files you will need the -->
<!-- allowed-ST-release-signers file published at -->
<!-- https://www.system-transparency.org/keys/, and run -->
<!-- ssh-keygen -Y verify -f allowed_signers -I releases@system-transparency.org -n file -s file.sig < file -->

NOTE: You will have to download a new boot loader image when the root
certificate for verifying OS packages expires every three months.

[UKI]: https://uapi-group.org/specifications/specs/unified_kernel_image/

## Boot in QEMU

This section shows how to run stboot and a prebuilt OS package in a VM on your Linux system.

The VM will want to store its EFI variables somewhere so create a file for NVRAM backing.

    cp /usr/share/OVMF/OVMF_VARS.fd OVMF_VARS.fd

Start the VM.

    qemu-system-x86_64 \
        -m 3G \
        -accel kvm \
        -accel tcg \
        -pidfile qemu.pid \
        -no-reboot \
        -nographic \
        -rtc base=localtime \
        -drive if=pflash,format=raw,file=/usr/share/OVMF/OVMF_CODE.fd,readonly=on \
        -drive if=pflash,format=raw,file=OVMF_VARS.fd \
        -object rng-random,filename=/dev/urandom,id=rng0 \
        -device virtio-rng-pci,rng=rng0 \
        -drive file="stboot.iso",format=raw,if=none,media=cdrom,id=drive-cd1,readonly=on \
        -device ahci,id=ahci0 -device ide-cd,bus=ahci0.0,drive=drive-cd1,id=cd1,bootindex=1

Note: All of the options used are not strictly necessary but none of
them should pose any problem, even when running in a VM or in a
container.

When the dust settles you should find yourself at a login prompt. Try
logging in as root using the password 'qa'.

```
Welcome to Debian GNU/Linux 12 (bookworm)!
...
amnesiac-debian login:
```

Hint: You can exit QEMU by pressing 'Ctrl-a x'.


## Boot some real hardware

This section shows two methods of using stboot to boot the OS package
on a physical machine. One where stboot.uki is read from a storage
medium like a disk or a USB stick. The other is using the [IPMI][]
[Baseboard Management Controller][] (BMC) for emulating a USB stick
containing stboot.iso.

WARNING: In order to run stboot you will have to disable [UEFI Secure
Boot][] which is generally not a good idea.

[UEFI Secure Boot]: https://en.wikipedia.org/wiki/UEFI#SECURE-BOOT
[Baseboard Management Controller]: https://en.wikipedia.org/wiki/Intelligent_Platform_Management_Interface#Baseboard_management_controller
[IPMI]: https://en.wikipedia.org/wiki/Intelligent_Platform_Management_Interface

#### Console output

The prebuilt images are sending console output to the first serial
port of the system being booted, at 115200 baud, 8 bits, no parity. In
order to enjoy the output you will want to use a server with remote
console access or connect a terminal (emulator) to the physical serial
port of the system.

#### Required infrastructure

A DHCP server is needed for the boot loader to configure the network
in order to be able to download the OS package from an IPv4 address.

Instructions for how to set this up is outside the scope of this guide.

### Booting from disk or USB stick

You will need a USB stick or disk (SSD or HDD) that you're comfortable
with creating an MS-DOS FAT partition on, as well as setting the
partition to bootable. The example below assumes that there's nothing
of value on the device and **will destroy its contents**.

#### Preparing boot media

This section prepares the boot media by copying the boot loader, in UKI format, to it.

WARNING: These instructions will **wipe the device** used for boot media.

Create partition table and boot partition.

    sudo parted -s -a opt -- /dev/sdX mklabel gpt mkpart EFI fat32 2048s 20M set 1 boot on

Create file system on boot partition.

    sudo mkfs.vfat /dev/sdX1

Mount file system.

    sudo mount /dev/sdX1 /mnt

Create directory structure for UEFI boot.

    sudo mkdir -p /mnt/EFI/BOOT

Copy stboot.uki to a [magically named file][].

    sudo cp stboot.uki /mnt/EFI/BOOT/BOOTX64.EFI

Unmount file sytem.

    sudo umount /mnt

[magically named file]: https://en.wikipedia.org/wiki/UEFI#Operating_systems

#### Boot the machine

Insert the boot media into the machine to boot and start the machine.

You might need to interrupt the ordinary boot sequence by pressing F11 or some other key depending on machine type.

The physical console will first display a single line reading

    EFI stub: Loaded initrd from LINUX_EFI_INITRD_MEDIA_GUID device path

and then it will take a long time before the login prompt is being displayed

    amnesiac-debian login:

If you're following the boot sequence **on the serial port** you
should first see a Linux kernel booting and then the following, in
order.

Information from stboot about configuring the network

    [INFO] DHCP successful - eth0

stboot downloading the OS package JSON file

    [INFO] Loading OS package via network
    [INFO] Downloading "https://st.glasklar.is/st/qa/qa-debian-bookworm-amd64.json"

stboot downloading the OS package ZIP file

    [INFO] Downloading "https://st.glasklar.is/st/qa/qa-debian-bookworm-amd64.zip"

stboot verifying the OS package

    [INFO] Processing OS package "qa-debian-bookworm-amd64.zip"
    [INFO] OS package passed verification

Switching over to the OS kernel

    [INFO] Loading boot image into memory
    [INFO] Handing over control - kexec
    kexec_core: Starting new kernel
    [    0.000000] Linux version 6.1.0-20-amd64 (debian-kernel@lists.debian.org) (gcc-12 (Debian 12.2.0-14) 12.2.0, GNU ld (GNU Binutils for Debian) 2.40) #1 SMP PREEMPT_DYNAMIC Debian 6.1.85-1 (2024-04-11)
    [    0.000000] Command line: console=ttyS0,115200n8 ro rdinit=/lib/systemd/systemd systemd.log_level=debug

Systemd starting

    [    2.933985] systemd[1]: systemd 252.22-1~deb12u1 running in system mode (+PAM +AUDIT +SELINUX +APPARMOR +IMA +SMACK +SECCOMP +GCRYPT -GNUTLS +OPENSSL +ACL +BLKID +CURL +ELFUTILS +FIDO2 +IDN2 -IDN +IPTC +KMOD +LIBCRYPTSETUP +LIBFDISK +PCRE2 -PWQUALITY +P11KIT +QRENCODE +TPM2 +BZIP2 +LZ4 +XZ +ZLIB +ZSTD -BPF_FRAMEWORK -XKBCOMMON +UTMP +SYSVINIT default-hierarchy=unified)

And eventually the login prompt

    amnesiac-debian login:

You can try logging in as user root and the password 'qa'. There's no
poweroff(8) command but systemctl poweroff should work.


### Boot off a virtual CD-ROM

The system to boot needs to be a server with a [BMC][] capable of
emulating a bootable device with an ISO. The following example is from
booting a Supermicro X11 with its BMC mounting the ISO containing
stboot over the [SMB][] network protocol.

[BMC]: https://en.wikipedia.org/wiki/Intelligent_Platform_Management_Interface#Baseboard_management_controller
[SMB]: https://en.wikipedia.org/wiki/Server_Message_Block

#### Prepare an SMB share

You will need to set up DHCP to hand out IPv4 configuration suitable
for reaching 192.168.0.1 to the BMC network interface.

You will also need to set up a Samba server serving `//192.168.0.2/public/stboot.iso`.

Instructions for how to set this up is outside the scope for this guide.

#### Configure the BMC and boot the server

1. Navigate to Virtual Media -> CD-ROM Image and enter the following values

  * Share Host: `192.168.0.1`
  * Path to Image: `\public\stboot.iso`

2. Press `Save`

3. Press `Mount`

4. Watch the `Device 1` line change from "No disk emulation set." to "There is an iso file mounted."

5. Reboot the server.
