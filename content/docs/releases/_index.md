---
title: Release process
weight: 9
bookCollapseSection: true
---
# Release process

The System Transparency project provides two types of releases:

  - **Component release**: essentially a git-tag that have a matching NEWS entry
    in its repository.
  - **Collection release**: a combination of component releases that are
    documented and tested to work together.  This is basically the source code
    of selected components and metadata packaged as a single tar archive.

Examples of components that are released include [stprov][], [stboot][], and
[stmgr][].  Component releases, collection releases, and verification of release
signatures are described separately in the following sections.

Note that we may make component releases without including them in a collection
release.  This gives the project a degree of freedom to experiment with new
features and breaking changes without affecting users that need something stable
to operate.  We are also considering various forms of binary releases. This may
lead to extended definitions of component releases and collection releases, or
entirely new release types.

[stprov]: https://git.glasklar.is/system-transparency/core/stprov
[stmgr]: https://git.glasklar.is/system-transparency/core/stmgr
[stboot]: https://git.glasklar.is/system-transparency/core/stboot
