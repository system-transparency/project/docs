#! /bin/bash
set -eu

# This script deploys the zip file compiled by CI/CD for a given git
# tag or git branch (default: main) by fetching it, unzipping it and
# copying the result to a webroot.

f() {
	local zipurl="$1"; shift
	local dest="$1"; shift
	tmpdir="$(mktemp -d)"
	trap "cd; rm -rf $tmpdir" EXIT
	cd "$tmpdir"
	curl -sL --head "$zipurl" | head -1 | tr -d '\r' |
	    (read -r proto http_code; [[ $http_code == 200 ]] || { echo "$zipurl => $proto $http_code"; exit 1; })
	curl -sL -o docs.sto.zip "$zipurl"
	unzip -qq docs.sto.zip
	mkdir -p "${dest}/"
	rsync -a --delete public/ "${dest}/"
}

gitref=${1:-main}
f "https://git.glasklar.is/api/v4/projects/88/jobs/artifacts/$gitref/download?job=publish_site" "/var/www/docs.system-transparency.org/$gitref"
