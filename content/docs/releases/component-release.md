---
title: Component release
weight: 1
---
## Component release

The System Transparency project develops, maintains, and releases the
source code of several git repositories.  A _component release_ is
defined as a git-tag which also has a matching NEWS entry in its
repository.  A tag without a matching NEWS entry should not to be
confused with a component release.  In other words, there are often many
_intermediate tags_ created during development and testing of new
features.  The component's NEWS file typically indicates such iteration
by having a work-in-progress entry where the version ends with `.X`,
e.g., `v0.4.X`.  An example of this is shown below, where the tags
`v0.4.0`, `v0.4.1`, and `v0.4.2` are intermediate tags before cutting a
release at tag `v0.4.3`.  The final tag gets signed by one of the
[release keys][].

{{< img src="component-release.svg" >}}

The NEWS file in each component lists information about all releases and
corresponding tags. The RELEASES.md file gives further information on a
component's (independent) release process.  Users that pick-and-chose
component releases are expected to pay careful attention to the
documented compatibility (or lack thereof), and to do their own
integration testing of the selected combination of components.

[release keys]: https://www.system-transparency.org/keys
